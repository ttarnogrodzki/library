from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.views.generic import CreateView, FormView, ListView, TemplateView
from django.urls import reverse_lazy
from .forms import CreateAuthorForm, RegisterForm
from .models import Author


class IndexView(TemplateView):
    """ Index view """
    template_name = "core/index.html"


index_view = IndexView.as_view()


class AuthorsView(ListView):
    """ Authors view """
    model = Author
    paginate_by = 10


authors_view = AuthorsView.as_view()


class CreateAuthorView(LoginRequiredMixin, CreateView):
    """ Create author view """
    model = Author
    success_url = reverse_lazy('core:authors')
    form_class = CreateAuthorForm
    login_url = reverse_lazy('login')


create_author_view = CreateAuthorView.as_view()


class RegisterView(FormView):
    """ Register form """
    template_name = 'registration/register.html'
    model = User
    form_class = RegisterForm
    success_url = reverse_lazy('core:index')

    def post(self, request, *args, **kwargs):
        response = super(RegisterView, self).post(request, *args, **kwargs)
        if response:
            form = self.get_form()
            form.save()
        return response


register_view = RegisterView.as_view()
