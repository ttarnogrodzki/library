from django.urls import path
from . import views

app_name = 'core'
urlpatterns = [
    path('', views.index_view, name='index'),
    path('register', views.register_view, name='register'),
    path('index', views.index_view, name='index'),
    path('authors', views.authors_view, name='authors'),
    path('author/create', views.create_author_view, name='create_author'),
]
