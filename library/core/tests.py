from django.test import TestCase
from django.urls import reverse_lazy
from math import ceil
from urllib.parse import quote
from .models import Author
from .mixins import TestMixin
from .views import CreateAuthorView, AuthorsView


class AuthorViewTests(TestMixin, TestCase):
    def test_view_without_authors(self):
        response = self.client.get(reverse_lazy('core:authors'))
        self.assertEqual(200, response.status_code)
        self.assertContains(response, "No authors")
        self.assertContains(response, "Authors")

    def test_view_authors(self):
        author_num = 20
        for _ in range(author_num):
            Author.objects.create(first_name=self.faker.name(), last_name=self.faker.last_name())

        response = self.client.get(reverse_lazy('core:authors'))
        paginator = response.context['page_obj'].paginator
        self.assertEqual(200, response.status_code)
        self.assertContains(response, "Authors")
        self.assertEqual(author_num, paginator.count, f"Unexpected number of authors: {paginator.count}")
        self.assertEqual(ceil(author_num/AuthorsView.paginate_by), paginator.num_pages,
                         f"Unexpected number of pages: {paginator.num_pages}")

    def test_add_author_without_login(self):
        target_url = reverse_lazy("core:create_author")
        response = self.client.get(target_url)
        expected_url = f"{reverse_lazy('login')}?next={quote(str(target_url))}"
        self.assertRedirects(response, expected_url)

    def test_add_author(self):
        user = self.get_or_create_user()
        author_data = {'first_name': self.faker.name(), 'last_name': self.faker.last_name()}

        self.client.force_login(user)
        response = self.client.post(reverse_lazy('core:create_author'), author_data)
        self.client.logout()

        self.assertRedirects(response, CreateAuthorView.success_url)
        self.assertTrue(Author.objects.filter(**author_data).exists())

