from django.contrib.auth.models import User
from faker import Faker


class TestMixin:
    """ Test mixin """
    faker = Faker()

    def get_or_create_user(self):
        """ GEt or create some user """
        name = self.faker.name()
        user, _ = User.objects.get_or_create(username=name, password=name)
        return user


