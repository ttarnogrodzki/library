from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.utils.translation import gettext_lazy as _
from django.forms import ModelForm
from .models import Author


class CreateAuthorForm(ModelForm):
    class Meta:
        model = Author
        fields = ['first_name', 'last_name']


class RegisterForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=False, help_text=_("Optional."), label=_("First name"))
    last_name = forms.CharField(max_length=30, required=False, help_text=_("Optional."), label=_("Last name"))
    email = forms.EmailField(max_length=64)

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2')
