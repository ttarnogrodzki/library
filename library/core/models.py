from datetime import date
from django.core.validators import MinLengthValidator
from django.db import models
from django.utils.translation import gettext_lazy as _


class Author(models.Model):
    """ Author class """
    first_name = models.CharField(_("First name"), max_length=128, validators=[MinLengthValidator(2)])
    last_name = models.CharField(_("Last name"), max_length=128, validators=[MinLengthValidator(2)])

    def name(self):
        return f"{self.first_name} {self.last_name}"

    def __str__(self):
        return self.name()

    class Meta:
        ordering = ['last_name', 'first_name', 'id']
        verbose_name = _("Author")
        verbose_name_plural = _("Authors")


class Book(models.Model):
    """ Book class """
    title = models.CharField(_("Title"), max_length=128, validators=[MinLengthValidator(2)])
    year = models.PositiveSmallIntegerField(_("Year"), choices=((y, y) for y in range(1950, date.today().year + 1)),
                                            default=date.today().year)

    authors = models.ManyToManyField(Author)

    class Meta:
        ordering = ['title']
        verbose_name = _("Book")
        verbose_name_plural = _("Books")
